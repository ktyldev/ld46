﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tumble : MonoBehaviour
{
    [SerializeField]
    [Range(0, 1)]
    private float _speed;

    private Quaternion _rotation;
    private Material _skybox;

    void OnEnable()
    {
        _rotation = Quaternion.Euler(new Vector3
        {
            x = Random.value,
            y = Random.value,
            z = Random.value
        }.normalized * _speed * 360f);
    }

    void LateUpdate()
    {
        transform.Rotate(_rotation.eulerAngles * Time.deltaTime);
    }
}
