﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: convert to ECS
public class AlarmLight : MonoBehaviour
{
    [SerializeField]
    private Light _light;
    [SerializeField]
    private float _speed = 1;

    private float _max;

    private void Awake()
    {
        _max = _light.intensity;
    }

    private void LateUpdate()
    {
        _light.intensity = Mathf.Clamp(Mathf.Sin(Time.time * _speed), 0, _max); 
    }
}
