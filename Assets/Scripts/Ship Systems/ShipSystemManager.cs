﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipSystemManager : MonoBehaviour
{
    [SerializeField] private ShipSystem[] _systems;

    private void LateUpdate()
    {
        float dt = Time.deltaTime;   
        foreach (ShipSystem system in _systems)
        {
            system.UpdateSystem(dt);
        }
    }
}
