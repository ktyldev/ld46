﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipPower : MonoBehaviour
{
    [SerializeField] private Float _power;
    [SerializeField] private GameEvent _powerOut;

    private void LateUpdate()
    {
        if (_power < 0)
        {
            _powerOut.Raise();
            this.enabled = false;
        }
    }
}
