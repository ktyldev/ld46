﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Lovell/Ship System")]
public class ShipSystem : ScriptableObject
{
    [SerializeField] private Float _shipPower;
    [SerializeField] private float _powerConsumption;
    public virtual float PowerConsumption => _isOn ? _powerConsumption : 0; 

    [System.Serializable]
    private class ConditionEvent
    {
        public MetaCondition condition;
        public GameEvent @event;

        public bool systemPower;
        public bool fireOnce = true;
        public bool Fired { get; set; } = false;
    }
    [SerializeField] private ConditionEvent[] _conditionEvents;

    private bool _isOn = true;

    public void UpdateSystem(float deltaTime)
    {
        UpdateConditions();

        // Update power
        _shipPower.Add(-_powerConsumption * deltaTime);
    }

    private void UpdateConditions()
    {
        foreach (ConditionEvent conditionEvent in _conditionEvents)
        {
            if (conditionEvent.condition.IsSatisfied)
            {
                if (conditionEvent.fireOnce && conditionEvent.Fired)
                {
                    continue;
                }

                conditionEvent.@event.Raise();
                conditionEvent.Fired = true;

                _isOn = conditionEvent.systemPower;
            }
        }
    }
}
