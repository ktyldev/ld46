﻿using Unity.Entities;
using UnityEngine;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Jobs;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Entities.UniversalDelegates;

public class PlayerController : MonoBehaviour
{
    PhysicsWorld PhysicsWorld => World.DefaultGameObjectInjectionWorld.GetExistingSystem<BuildPhysicsWorld>().PhysicsWorld;
    EntityManager EntityManager => World.DefaultGameObjectInjectionWorld.EntityManager;

    private const string MOUSE_X = "Mouse X";
    private const string MOUSE_Y = "Mouse Y";

    [Header("References")]
    [SerializeField] private Camera _camera;

    [Header("Interaction")]
    [SerializeField] private float _interactionDistance;

    [Header("Mouse Look")]
    [SerializeField] private float _sensitivityX;
    [SerializeField] private float _sensitivityY;

    [SerializeField] private float _minX;
    [SerializeField] private float _maxX;

    [SerializeField] private float _minY;
    [SerializeField] private float _maxY;

    [Header("Debug")]
    [SerializeField] private bool _lockCursor;

    float _rotationX;
    float _rotationY;

    Quaternion _originalRotation;

    private void Start()
    {
        _originalRotation = transform.localRotation;
        Cursor.visible = false;
        if (_lockCursor)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    void Update()
    {
        UpdateLook();
        UpdateInteraction();
    }

    private void UpdateLook()
    {
        _rotationX += Input.GetAxis( MOUSE_X ) * _sensitivityX;
        _rotationY += Input.GetAxis( MOUSE_Y ) * _sensitivityY;

        _rotationX = ClampAngle( _rotationX, _minX, _maxX );
        _rotationY = ClampAngle( _rotationY, _minY, _maxY );

        var quaternionY = Quaternion.AngleAxis( _rotationY, Vector3.left );
        var quaternionX = Quaternion.AngleAxis( _rotationX, Vector3.up );

        transform.localRotation = _originalRotation * quaternionX * quaternionY;
    }

    private void UpdateInteraction()
    {
        bool interacted = Input.GetMouseButtonDown( 0 );

        var ray = _camera.ViewportPointToRay( new Vector3( 0.5f, 0.5f ) );

        var rayInput = new RaycastInput
        {
            Start = ray.origin,
            End = ray.GetPoint( _interactionDistance ),
            Filter = CollisionFilter.Default,
        };

        //if (PhysicsWorld.CastRay( rayInput, out Unity.Physics.RaycastHit hit ) == false) return;

        if ( interacted == true)
        {
            if (Physics.Raycast(ray, out UnityEngine.RaycastHit hit))
            {
                if (hit.collider.TryGetComponent(out Interactable interactable))
                {
                    interactable.Interact();
                } 
            }
        }
    }

    static float ClampAngle( float angle, float min, float max )
    {
        angle %= 360.0f;

        if ((angle >= -360.0f) && (angle <= 360.0f))
        {
            if (angle < -360.0f) angle += 360.0f;
            if (angle > 360.0f) angle -= 360.0f;
        }

        return Mathf.Clamp( angle, min, max );
    }
}
