﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Toggle : Interactable
{
    protected override bool StateChangeEvents => true;

    private void ToggleActive() => Active ^= true;

    protected abstract void UpdateGraphics();

    protected override void OnEnable()
    {
        if (Random.value < 0.5f)
        {
            ToggleActive();
        }

        UpdateGraphics();

        base.OnEnable();
    }

    public override void Interact()
    {
        base.Interact();

        ToggleActive();
        UpdateGraphics();
    }
}
