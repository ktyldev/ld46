﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(Switch))]
public class SwitchEditor : InteractableEditor
{
}

#endif

public class Switch : Toggle
{
    [Header("Lever")]
    [SerializeField] private Transform _lever;
    [SerializeField] private float _angle;

    [Header("Light")]
    [SerializeField] private Renderer _lightRenderer;
    [SerializeField] private float _intensity;

    private MaterialPropertyBlock _props = null;

    private const string _emissionColour = "_EmissionColor";

    public override string InteractableName => (name + " Switch");

    public override InteractableType InteractableType => InteractableType.Switch;

    protected override void UpdateGraphics()
    {
        UpdateLight();
        UpdateLever();
    }

    private void UpdateLight()
    {
        // Lazy init
        if (_props == null)
        {
            _props = new MaterialPropertyBlock();
        }

        Color colour = _lightRenderer.material.GetColor(_emissionColour);
        _props.SetColor(_emissionColour, colour * (Active ? _intensity : 0));
        _lightRenderer.SetPropertyBlock(_props);
    }

    private void UpdateLever()
    {
        float angle = Active ? _angle : -_angle;
        _lever.transform.localRotation = Quaternion.Euler(angle, 0, 0);
    }
}
