﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : Interactable
{
    [Header("Button")]
    [SerializeField] private Transform _button;
    [SerializeField] private float _depth;
    [SerializeField] private float _interactTime;

    private Vector3 _startPos;
    private WaitForSeconds _wait;

    protected override bool StateChangeEvents => true;

    public override InteractableType InteractableType => InteractableType.Button;

    public override string InteractableName => (name + " Button");

    private void Awake()
    {
        _startPos = transform.position;
        _wait = new WaitForSeconds(_interactTime);
    }

    public override void Interact()
    {
        base.Interact();

        StartCoroutine(Press());
    }

    private IEnumerator Press()
    {
        Active = true;
        UpdateButton();

        yield return _wait;

        Active = false;
        UpdateButton();
    }

    private void UpdateButton()
    {
        UpdateGraphics();
        PlaySound();
    }

    private void UpdateGraphics()
    {
        transform.position = Active
            ? _startPos - transform.up * _depth
            : _startPos;
    }
}
