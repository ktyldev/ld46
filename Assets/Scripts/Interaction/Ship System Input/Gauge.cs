﻿using UnityEngine;

public class Gauge : Interactable
{
    private static readonly WaitForSeconds[] _waits =
    {
        new WaitForSeconds(0.5f),
        new WaitForSeconds(1.0f),
        new WaitForSeconds(2.5f),
        new WaitForSeconds(5.0f),
        new WaitForSeconds(7.5f),
    };

    protected override bool StateChangeEvents => false;

    public override InteractableType InteractableType => InteractableType.Switch;

	public override string InteractableName => (name + " Gauge");

	[SerializeField]
    private Transform _pivotTransform = null;

    private float _targetPivotAngle;

    private float _rotationSpeed = 10.0f;

    public override void Interact()
    {
        Active = true;

        base.Interact();
    }

    public void SetUnchecked()
    {
        Active = false;
    }

    private void Awake()
    {
        System.Collections.IEnumerator TargetPivotUpdateLoop()
        {
            for (;;)
            {
                _targetPivotAngle = ((360.0f * Random.value) - 180);
                _rotationSpeed = Random.Range(10.0f, 100.0f);

                yield return _waits[Random.Range(0, _waits.Length)];
            }
        }

        StartCoroutine(TargetPivotUpdateLoop());
    }

    private void Update()
    {
        _pivotTransform.Rotate(0f, (_rotationSpeed *
                ((_targetPivotAngle < 0.0f) ? -Time.deltaTime : Time.deltaTime)), 0f);
    }
}
