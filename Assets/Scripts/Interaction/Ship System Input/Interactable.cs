﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(Interactable))]
public class InteractableEditor : Editor
{
    private Interactable _data;

    private void OnEnable()
    {
        _data = target as Interactable;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Toggle highlight"))
        {
            _data.Highlight ^= true; // toggle
        }
    }
}

public abstract partial class Interactable : MonoBehaviour
{
    public InteractableCondition[] Conditions
    {
        get => _conditions;
        set => _conditions = value;
    }
}
#endif

public abstract partial class Interactable : MonoBehaviour
{
    [SerializeField] private InteractableCondition[] _conditions;

    [SerializeField] private HighlightData _highlightData;

    private bool _active;
    public bool Active
    {
        get => _active;
        protected set
        {
            if (_active != value)
            {
                _active = value;
                if (StateChangeEvents)
                {
                    stateChanged?.Invoke();
                }
            }
        }
    }

    public abstract string InteractableName { get; }

    public abstract InteractableType InteractableType { get; }

    protected abstract bool StateChangeEvents { get; }

    private const string _emissionColour = "_EmissionColor";
    private bool _highlight;
    public bool Highlight
    {
        get => _highlight;
        set
        {
            _highlight = value;

            foreach (RenderData rd in _renderData)
            {
                Color c = rd.startEmission;

                if (_highlight)
                {
                    c = _highlightData.Colour;
                }

                rd.props.SetColor(_emissionColour, c);
                rd.renderer.SetPropertyBlock(rd.props);
            }
        }
    }

    public virtual void Interact()
    {
        Highlight = false;

        PlaySound();

        interacted?.Invoke();
    }

    private void LateUpdate()
    {
        foreach (RenderData renderData in _renderData)
        {
            if (renderData.renderer == null)
            {
                Debug.LogError("wtf");
                return;
            }
        }

        if (Highlight)
        {
            foreach (RenderData rd in _renderData)
            {
                Color c = rd.startEmission;

                if (_highlight)
                {
                    c = Color.Lerp(
                        rd.startEmission,
                        _highlightData.Colour,
                        Mathf.Clamp(Mathf.Sin(Time.time * _highlightData.Speed), 0, 1));
                }

                rd.props.SetColor(_emissionColour, c);
                rd.renderer.SetPropertyBlock(rd.props);
            }
        }
    }

    protected virtual void PlaySound()
    {
    }

    public delegate void OnStateChanged();
    public event OnStateChanged stateChanged;

    public delegate void OnInteracted();
    public event OnInteracted interacted;


    private class RenderData
    {
        public Renderer renderer;
        public Color startEmission;
        public MaterialPropertyBlock props;
    }
    private readonly List<RenderData> _renderData = new List<RenderData>();

    protected virtual void OnEnable()
    {
    }

    private void Start()
    {
        foreach (Renderer renderer in GetComponentsInChildren<Renderer>())
        {
            RenderData renderData = new RenderData
            {
                renderer = renderer,
                startEmission = renderer.material.GetColor(_emissionColour),
                props = new MaterialPropertyBlock()
            };

            _renderData.Add(renderData);
        }

        foreach (InteractableCondition condition in _conditions)
        {
            if (condition)
            {
                condition.RegisterInteractable(this);
            }
        }
    }
}
