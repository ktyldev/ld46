
using UnityEngine;

public class Valve : Toggle
{
    private Quaternion _startRot;

    private Quaternion _modRot;

    public override string InteractableName => (name + " Valve");

    public override InteractableType InteractableType => InteractableType.Valve;

    private void Awake()
    {
        _startRot = transform.localRotation;
        _startRot = transform.localRotation * Quaternion.Euler(0, 90, 0);
    }

    protected override void UpdateGraphics()
    {
        transform.localRotation = Active
            ? _startRot
            : _modRot;
    }
}
