using UnityEngine;

public class InteractableNameGenerator
{
    private const int _namesListMax = 72;

    private string[] _namesList = new string[_namesListMax]
    {
        "Flange", "Oxygenation", "Matrix", "Dyson", "Esper", "Humidification",
        "Secondary", "Auxillary", "Combustion", "Twister", "Beeper", "Booper",
        "Boopboop", "Oscillation", "Gravitation", "Combiner", "Switcher", "Swapper",
        "Spicer", "Discharge", "Disengagement", "Attention Retention", "Enthusiasm",
        "Thingme", "Whatever-its-Called", "Single-use", "Neutron", "Harvest", "Vectorization",
        "Reverse-Power", "Water", "Fire", "Warp", "Silent Alarm", "Clock",
        "Radiation", "Time", "High-Quality", "Rusted", "Welding", "Old",
        "Broken", "Accelerator", "Drive", "Parking", "Singularity", "Teraflop",
        "Void", "Data", "Celestial", "Artificial", "Ephemeral", "Detonator",
        "Landing", "Spinning", "Fixer", "Mixer", "Blender", "Pressure",
        "Hull", "Harmonizer", "Overdrive", "Morph", "Contact", "Worker",
        "Heavy-Duty", "Coolant", "Dry", "Wet", "Unsafe", "Experimental", "Systems"
    };

    private int _namesListCount = _namesListMax;

    public string GiveRandom()
    {
        if (_namesListCount != 0)
        {
            int newNamesListCount = (_namesListCount - 1);
            int randomIndex = Random.Range(0, _namesListCount);
            string randomName = _namesList[randomIndex];
            _namesList[randomIndex] = _namesList[newNamesListCount];
            _namesListCount = newNamesListCount;

            return randomName;
        }

        return "";
    }

    public string PickRandom()
    {
        return _namesList[Random.Range(0, _namesList.Length)];
    }
}
