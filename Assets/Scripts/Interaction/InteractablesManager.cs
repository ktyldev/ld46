﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(InteractablesManager))]
public class InteractablesManagerEditor : Editor
{
    private InteractablesManager _data;

    private void OnEnable()
    {
        _data = target as InteractablesManager;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Start narrative"))
        {
            _data.StartNarrative();
        }
    }
}

#endif

public class InteractablesManager : MonoBehaviour
{
    [SerializeField] private Narrative _narrative;
    [SerializeField] private InteractablesList _interactablesList;

    private readonly InteractableNameGenerator _nameGenerator = new InteractableNameGenerator();

    private void Start()
    {
        Shader.EnableKeyword("_EMISSION");

        foreach (Interactable interactable in GetComponentsInChildren<Interactable>())
        {
            interactable.name = _nameGenerator.GiveRandom();
            interactable.interacted += _narrative.CheckTask;

            _interactablesList.Interactables.Add(interactable);
        }

        StartNarrative();
    }

    public void StartNarrative()
    {
        StartCoroutine(NarrativeRoutine());
    }

    private IEnumerator NarrativeRoutine()
    {
        yield return new WaitForSeconds(1);
        _narrative.Start();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            HighlightableCondition hc = _interactablesList.GenerateCondition();

            hc.highlightable.Highlight = true;
        }
    }
}
