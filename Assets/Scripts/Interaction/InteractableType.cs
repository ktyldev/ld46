﻿using System;

[Flags] public enum InteractableType
{
    NONE = 0,

    Switch = 1,
    Button = 2,
    Gauge = 4,
    Valve = 8
}
