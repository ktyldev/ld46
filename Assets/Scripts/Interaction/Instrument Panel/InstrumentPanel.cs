﻿
#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Text;

[CustomEditor(typeof(InstrumentPanel))]
public class InstrumentPanelEditor : Editor
{
    private InstrumentPanel _data;

    private void OnEnable()
    {
        _data = target as InstrumentPanel;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Update Pattern"))
        {
            _data.UpdatePattern();
        }
        if (GUILayout.Button("Update All"))
        {
            InstrumentPanel.UpdateAll();
        }
    }
}

#region Statics
public partial class InstrumentPanel
{
    public static void UpdateAll()
    {
        foreach (InstrumentPanel panel in FindObjectsOfType<InstrumentPanel>())
        {
            panel.UpdatePattern();
        }
    }
}
#endregion


[ExecuteAlways]
public partial class InstrumentPanel : MonoBehaviour
{
    [SerializeField] private Transform[] _slots;
    [SerializeField] private InteractablePrefabCollection _prefabs;
    [SerializeField] private string _pattern;

    private const string _defaultPattern = "SSSSSS";

    private string Pattern
    {
        get
        {
            if (string.IsNullOrEmpty(_pattern) || _pattern.Length != _defaultPattern.Length)
            {
                return _defaultPattern;
            }

            string reverse = "";
            for (int i = _pattern.Length - 1; i >= 0; i--)
            {
                reverse += _pattern[i].ToString();
            }

            return reverse;
        }
    }

    private readonly List<InteractableCondition[]> _conditionCache = new List<InteractableCondition[]>();

    private void OnEnable()
    {
        UpdatePattern();
    }

    private struct TypeMap
    {
        public char c;
        public InteractableType t;
    }
    private readonly TypeMap[] _map = new[]
    {
        new TypeMap { c = 'S', t = InteractableType.Switch },
        new TypeMap { c = 'B', t = InteractableType.Button },
        new TypeMap { c = 'G', t = InteractableType.Gauge },
        new TypeMap { c = 'V', t = InteractableType.Valve }
    };

    public void UpdatePattern()
    {
        Clear();

        for (int i = 0; i < Pattern.Length; i++)
        {
            char c = Pattern[i];

            foreach (TypeMap map in _map)
            {
                if (c == map.c)
                {
                    AddInteractableToSlot(i, map.t);
                    continue;
                }
            }
        }
    }

    private void AddInteractableToSlot(int i, InteractableType t)
    {
        GameObject obj = Instantiate(_prefabs[t], _slots[i]);
        if (obj.TryGetComponent(out Interactable interactable))
        {
            interactable.Conditions = _conditionCache[i];
        }
    }

    public void Clear()
    {
        _conditionCache.Clear();

        for (int i = 0; i < _slots.Length; i++)
        {
            Interactable interactable = _slots[i].GetComponentInChildren<Interactable>();
            _conditionCache.Add(interactable ? interactable.Conditions : null);

            ClearSlot(_slots[i]);
        }
    }

    private void ClearSlot(Transform slot)
    {
        for (int i = slot.childCount - 1; i >= 0; i--)
        {
            GameObject target = slot.GetChild(i).gameObject;

            DestroyImmediate(target);
        }
    }
}

#endif  
