﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Lovell/Instrument Panel Pattern")]
public class InstrumentPanelPattern : ScriptableObject
{
    [SerializeField] private InteractableType[] _pattern;
    public InteractableType this[int i] => _pattern[i];

    public int Length => _pattern.Length;
}
