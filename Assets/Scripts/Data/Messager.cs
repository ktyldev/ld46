﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Lovell/Messager")]
public class Messager : ScriptableObject
{
    public delegate void OnMessage(string message);
    public event OnMessage onMessage;

    public void SendMessage(string message)
    {
        onMessage?.Invoke(message);
    }
}
