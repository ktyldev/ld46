﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Lovell/Data/Interactable Prefab Collection")]
public class InteractablePrefabCollection : ScriptableObject
{
    [SerializeField] private GameObject _button;
    public GameObject Button => _button;

    [SerializeField] private GameObject _switch;
    public GameObject Switch => _switch;

    [SerializeField] private GameObject _gauge;
    public GameObject Gauge => _gauge;

    [SerializeField] private GameObject _valve;
    public GameObject Valve => _valve;

    public GameObject this[InteractableType interactableType]
    { 
        get
        {
            switch (interactableType)
            {
                case InteractableType.Switch: return Switch;
                case InteractableType.Button: return Button;
                case InteractableType.Gauge: return Gauge;
                case InteractableType.Valve: return Valve;
                default: return null;
            }
        }
    }
}
