﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Lovell/Data/Prefab Collection")]
public class PrefabCollection : ScriptableObject
{
    [SerializeField] private GameObject[] _prefabs;

    public int Length => _prefabs.Length;

    public GameObject this[int index] => _prefabs[index];
}
