﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Lovell/Data/Task")]
public class Task : ScriptableObject
{
    [System.Serializable]
    private class TaskStage : ICondition
    {
        [SerializeField] private InteractableCondition _interactableCondition;
        public string startMessage;
        public string endMessage;

        private ICondition _condition;
        private IHighlightable _highlightable;

        public ICondition Condition 
        { 
            get
            {
                return _interactableCondition != null
                    ? _interactableCondition
                    : _condition;
            }
            set => _condition = value;
        }

        public IHighlightable Highlightable
        {
            get
            {
                return _interactableCondition != null
                    ? _interactableCondition
                    : _highlightable;
            }
            set => _highlightable = value;
        }

        public bool IsSatisfied => Condition.IsSatisfied;
    }
    [SerializeField] private TaskStage[] _stages;
    [SerializeField] private GameEvent _taskCompleted;
    [SerializeField] private InteractablesList _interactables;

    private Messager _messager;

    public delegate void OnCompleted();
    public event OnCompleted completed;

    private readonly List<Interactable> _controls = new List<Interactable>();
    private readonly Queue<TaskStage> _stageQueue = new Queue<TaskStage>();

    public void Init()
    {
        foreach (TaskStage taskStage in _stages)
        {
            if (taskStage.Condition != null)
            {
                HighlightableCondition cond = _interactables.GenerateCondition();

                taskStage.Condition = cond.condition;
                taskStage.Highlightable = cond.highlightable;
            }
        }
    }

    public void Advance()
    {
        TaskStage last = _stageQueue.Peek();
        _messager.SendMessage(last.endMessage);

        _stageQueue.Dequeue();
        if (_stageQueue.Count == 0)
        {
            completed?.Invoke();
            _taskCompleted.Raise();
        }
    }

    private ICondition GenerateRandomCondition(int controlsCount)
    {
        _controls.Clear();

        while (_controls.Count < controlsCount)
        {
            Interactable interactable = GetRandomInteractable();
            if (!_controls.Contains(interactable)) 
            {
                _controls.Add(interactable);
            }
        }

        return null;
    }

    private Interactable GetRandomInteractable()
    {
        int r = Random.Range(0, _interactables.Interactables.Count);
        Interactable interactable = _interactables.Interactables[r];

        return interactable;
    }

    public override string ToString() => name;

    public void Init(Messager messager)
    {
        _messager = messager;

        // queue stages
        foreach (TaskStage stage in _stages)
        {
            _stageQueue.Enqueue(stage);
        }

        TaskStage current = _stageQueue.Peek();

        _messager.SendMessage(current.startMessage);
        current.Highlightable.Highlight = true;
    }

    public void Check()
    {
        TaskStage current = _stageQueue.Peek();
        if (current.IsSatisfied)
        {
            Advance();
        }
    }
}
