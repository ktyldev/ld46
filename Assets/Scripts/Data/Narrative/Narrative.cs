﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(Narrative))]
public class NarrativeEditor : Editor
{
    private Narrative _data;

    private void OnEnable()
    {
        _data = target as Narrative;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GUILayout.Label($"Current Task: {_data.CurrentTask}");
    }
}

#endif

[CreateAssetMenu(menuName = "Lovell/Data/Narrative")]
public class Narrative : ScriptableObject
{
    [SerializeField] private Task[] _tasks;
    [SerializeField] private InteractablesList _interactables;
    [SerializeField] private Messager _messager;

    public Task[] Tasks => _tasks;

    private readonly Queue<Task> _taskQueue = new Queue<Task>();

    private int _taskIndex = 0;

    public Task CurrentTask
    { 
        get
        {
            if (_taskQueue.Count == 0)
            {
                return null;
            }

            return _taskQueue.Peek();
        }
    }

    public void Start()
    {
        // queue tasks
        foreach (Task task in _tasks)
        {
            task.completed += UpdateTask;
            _taskQueue.Enqueue(task);
        }

        CurrentTask.Init(_messager);
    }

    public void CheckTask()
    {
        if (CurrentTask == null)
        {
            Debug.LogError("no current task");
            return;
        }

        Debug.Log("check task");

        CurrentTask.Check();
    }

    private bool _done;
    public void UpdateTask()
    {
        Debug.Log("update task");

        if (_done)
        {
            return;
        }

        Task last = _taskQueue.Dequeue();

        if (_taskQueue.Count == 0)
        {
            _done = true;
            Debug.Log("All tasks completed!");
            return;
        }

        CurrentTask.Init(_messager);
    }
}
