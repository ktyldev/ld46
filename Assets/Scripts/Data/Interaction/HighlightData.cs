﻿using UnityEngine;

[CreateAssetMenu(menuName = "Lovell/Data/Interactable Highlight Data")]
public class HighlightData : ScriptableObject
{
    [SerializeField] private Color _colour;
    [SerializeField] private float _intensity;
    [SerializeField] private float _speed;

    public float Speed => _speed;

    public Color Colour => _colour * _intensity;
}
