﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(InteractablesList))]
public class InteractablesListEditor : Editor
{
    private InteractablesList _data;

    private void OnEnable()
    {
        _data = target as InteractablesList;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GUILayout.Label($"Count: {_data.Interactables.Count}");
    }
}

#endif

public struct HighlightableCondition
{
    public ICondition condition;
    public IHighlightable highlightable;
}

[CreateAssetMenu(menuName = "Lovell/Data/Interactables List")]
public class InteractablesList : ScriptableObject
{
    [System.Serializable]
    private struct InteractablesPerCondition
    {
        public int min;
        public int max;
    }
    [SerializeField] private InteractablesPerCondition _interactablesPerCondition;

    private readonly List<Interactable> _interactables = new List<Interactable>();
    public List<Interactable> Interactables 
    {
        get => _interactables; 
        set
        {
            _interactables.Clear();
            _interactables.AddRange(value);
        }
    }

    private List<Interactable> _conditionInteractables = new List<Interactable>();

    public HighlightableCondition GenerateCondition()
    {
        _conditionInteractables.Clear();
        int interactableCount = Random.Range(_interactablesPerCondition.min, _interactablesPerCondition.max);

        while (_conditionInteractables.Count < interactableCount)
        {
            Interactable interactable = GetRandomInteractable();
            if (!_conditionInteractables.Contains(interactable))
            {
                // limit to one button per condition
                bool button = false;
                foreach (Interactable i in _conditionInteractables)
                {
                    if (i.InteractableType == InteractableType.Button)
                    {
                        button = true;
                        break;
                    }
                }

                if (!button)
                {
                    _conditionInteractables.Add(interactable);
                }
            }
        }

        var vals = System.Enum.GetValues(typeof(GroupOp));
        GroupOp op = (GroupOp)vals.GetValue(Random.Range(0, vals.Length));

        RuntimeCondition condition = new RuntimeCondition(_conditionInteractables, op);

        return new HighlightableCondition
        {
            condition = condition,
            highlightable = condition
        };
    }

    private Interactable GetRandomInteractable()
    {
        int r = Random.Range(0, Interactables.Count);
        return Interactables[r];
    }
}
