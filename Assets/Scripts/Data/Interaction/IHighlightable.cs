﻿public interface IHighlightable
{
    bool Highlight { set; }
}
