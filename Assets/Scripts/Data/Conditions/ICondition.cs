﻿public interface ICondition
{
    bool IsSatisfied { get; }
}
