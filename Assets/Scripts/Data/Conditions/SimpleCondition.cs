﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Lovell/Conditions/Simple")]
public class SimpleCondition : Condition
{
    [SerializeField] private bool _isSatisfied;

    public override bool IsSatisfied => _isSatisfied;
}
