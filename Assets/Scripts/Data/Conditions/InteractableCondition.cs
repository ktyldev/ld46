﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#region Editor
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(InteractableCondition))]
public class InteractableConditionEditor : Editor
{
    private InteractableCondition _data;

    private void OnEnable()
    {
        _data = target as InteractableCondition;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GUILayout.Label($"Interactables: {_data.InteractableCount}");
        GUILayout.Label($"Is satisfied: {_data.IsSatisfied}");
        foreach (var interactable in _data.Interactables)
        {
            GUILayout.Label($"{interactable}: {interactable.Active}");
        }
    }
}

#endif
#endregion

[CreateAssetMenu(menuName = "Lovell/Interactable Condition")]
public class InteractableCondition : Condition, IHighlightable
{
    public enum BooleanOps
    {
        All,
        Any,
        None
    }
    [SerializeField]
    private BooleanOps _boolOp;

    private readonly List<Interactable> _interactables = new List<Interactable>();
    public int InteractableCount => _interactables.Count;
    public List<Interactable> Interactables => _interactables;

    public override bool IsSatisfied
    {
        get
        {
            switch (_boolOp)
            {
                case BooleanOps.All: return All();
                case BooleanOps.Any: return Any();
                case BooleanOps.None: return None();
                default: return false;
            }
        }
    }

    public bool Highlight
    {
        set
        {
            foreach (Interactable interactable in _interactables)
            {
                interactable.Highlight = value;
            }
        }
    }

    private bool All()
    {
        foreach (Interactable interactable in _interactables)
        {
            if (!interactable.Active)
            {
                return false;
            }
        }

        return true;
    }

    private bool Any()
    {
        foreach (Interactable interactable in _interactables)
        {
            if (interactable.Active)
            {
                return true;
            }
        }

        return false;
    }

    private bool None()
    {
        foreach (Interactable interactable in _interactables)
        {
            if (interactable.Active)
            {
                return false;
            }
        }

        return true;
    }

    public void RegisterInteractable(Interactable interactable)
    {
        Debug.Log($"{name} register {interactable}");

        _interactables.Add(interactable);

        interactable.stateChanged += () => Debug.Log($"{name} condition updated: {IsSatisfied}");
    }
}
