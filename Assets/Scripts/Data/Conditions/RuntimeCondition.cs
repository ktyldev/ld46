﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuntimeCondition : ICondition, IHighlightable
{
    private List<Interactable> _interactables;
    private GroupOp _op;

    public RuntimeCondition(List<Interactable> interactables, GroupOp op)
    {
        _interactables = interactables;
        _op = op;
    }

    public bool IsSatisfied => throw new System.NotImplementedException();

    public bool Highlight
    {
        set
        {
            foreach (Interactable interactable in _interactables)
            {
                interactable.Highlight = value;
            }
        }
    }
}
