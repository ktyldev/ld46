﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Lovell/Conditions/Compare Conditions")]
public class CompareCondition : Condition
{
    private enum BoolOp
    {
        Is,
        IsNot
    }

    [SerializeField] private Condition _condition1;
    [SerializeField] private BoolOp _op;
    [SerializeField] private Condition _condition2;

    public override bool IsSatisfied
    { 
        get
        {
            switch (_op)
            {
                case BoolOp.Is:
                    return _condition1.IsSatisfied == _condition2.IsSatisfied;

                case BoolOp.IsNot:
                    return _condition1.IsSatisfied != _condition2.IsSatisfied;

                default:
                    return false;
            }
        }
    }
}
