﻿public enum GroupOp
{
    All,
    Any,
    None
}

public static class ConditionExstensions
{
    public static bool All(this ICondition[] conditions)
    {
        foreach (ICondition condition in conditions)
        {
            if (!condition.IsSatisfied)
            {
                return false;
            }
        }

        return true;
    }

    public static bool Any(this ICondition[] conditions)
    {
        foreach (ICondition condition in conditions)
        {
            if (condition.IsSatisfied)
            {
                return true;
            }
        }

        return false;
    }

    public static bool None(this ICondition[] conditions)
    {
        foreach (ICondition condition in conditions)
        {
            if (condition.IsSatisfied)
            {
                return false;
            }
        }

        return true;
    }
}
