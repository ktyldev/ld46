﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Condition : ScriptableObject, ICondition
{
    public abstract bool IsSatisfied { get; }
}
