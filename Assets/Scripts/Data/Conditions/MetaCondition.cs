﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Lovell/Conditions/Meta")]
public class MetaCondition : Condition
{
    [SerializeField] private Condition[] _conditions;

    public override bool IsSatisfied
    {
        get
        {
            foreach (Condition condition in _conditions)
            {
                if (!condition.IsSatisfied)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
