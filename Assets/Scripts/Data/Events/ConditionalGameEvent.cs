﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Lovell/Data/Conditional Game Event")]
public class ConditionalGameEvent : GameEvent
{
    [SerializeField] private List<GameEvent> _predicateEvents;

    private void OnEnable()
    {
        if (_predicateEvents == null || _predicateEvents.Count == 0)
        {
            return;
        }

        foreach (GameEvent e in _predicateEvents)
        {
            e.raised += CheckCondition;
        }
    }

    private void CheckCondition()
    {
        foreach (GameEvent gameEvent in _predicateEvents)
        {
            if (gameEvent.RaisedCount < 1)
            {
                return;
            }
        }

        Raise();
    }
}
