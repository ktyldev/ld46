﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#region Editor
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(GameEvent))]
public class GameEventEditor : Editor
{
    private GameEvent _data;

    private void OnEnable()
    {
        _data = target as GameEvent;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Raise"))
        {
            _data.Raise();
        }
    }
}
#endif
#endregion


[CreateAssetMenu(menuName = "Lovell/Data/Game Event")]
public class GameEvent : ScriptableObject
{
    [SerializeField] private bool _log;

    private readonly List<GameEventListener> _listeners = new List<GameEventListener>();

    public delegate void GameEventRaised();
    public event GameEventRaised raised;

    public int RaisedCount { get; private set; } = 0;

    private void OnEnable()
    {
        RaisedCount = 0;
    }

    public void Raise()
    {
        RaisedCount++;

        if (_log)
        {
            Debug.Log($"{this} raised", this);
        }

        foreach (GameEventListener listener in _listeners)
        {
            listener.OnEventRaised();
        }

        raised?.Invoke();
    }

    public void AddListener(GameEventListener listener)
    {
        if (_listeners.Contains(listener))
        {
            Debug.LogError($"Listener already registered for {this}", this);
            return;
        }

        _listeners.Add(listener);
    }

    public void RemoveListener(GameEventListener listener)
    {
        if (!_listeners.Contains(listener))
        {
            Debug.LogError($"Listener not registered for {this}", this);
            return;
        }

        _listeners.Remove(listener);
    }
}
