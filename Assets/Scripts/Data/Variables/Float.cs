﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#region Editor
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(Float))]
public class FloatEditor : Editor
{
    private Float _data;

    private void OnEnable()
    {
        _data = target as Float;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GUILayout.Label($"Current: {_data.Value}");
    }
}
#endif
#endregion

[CreateAssetMenu(menuName = "Lovell/Variables/Float")]
public class Float : ScriptableObject
{
    [SerializeField] private float _value;

    public float Value { get; private set; }

    public static implicit operator float(Float f) => f.Value;

    private void OnEnable()
    {
        Value = _value;
    }

    public void Add(float amount)
    {
        Value += amount;
    }
}
