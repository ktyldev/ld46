﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class MissonControl : MonoBehaviour
{
    [SerializeField] private Narrative _narrative;
    [SerializeField] private Messager _messager;

    private readonly StringBuilder _sb = new StringBuilder();
    private Text _text;

    private void Awake()
    {
        _text = GetComponent<Text>();
    }

    private void OnEnable()
    {
        _messager.onMessage += WriteMessage;
    }

    private void WriteMessage(string message)
    {
        _sb.AppendLine(message);        
    }

    private void OnGUI()
    {
        _text.text = _sb.ToString();
    }
}
