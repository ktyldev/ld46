﻿Shader "Custom/depth"
{
    Properties
    {
        _Colour ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0

        _NearColour ("Near Colour", Color) = (0,0,0,0)
        _FarColour ("Far Colour", Color) = (0,0,0,0)
        _DepthMidpoint("Depth Midpoint", Float) = 1
        _MaxDepth ("Max Depth", Float) = 2
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
            float3 worldPos;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Colour;

        fixed4 _NearColour;
        fixed4 _FarColour;
        float _DepthMidpoint;
        float _MaxDepth;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            float distanceToCamera = distance(IN.worldPos, _WorldSpaceCameraPos);

            // how much near
            // proportion of distance to cam to depth
            float near = clamp(distanceToCamera / _DepthMidpoint, 0, 1);
            float nearColour = lerp(_NearColour, _Colour, near);

            // how much far
            float d = (distanceToCamera - _DepthMidpoint) / (_MaxDepth - _DepthMidpoint);
            float far = clamp(d, 0, 1);
            float farColour = lerp(_Colour, _FarColour, far);

			fixed4 c = lerp(nearColour, farColour, distanceToCamera / _MaxDepth);

			// Albedo comes from a texture tinted by color
            //fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
